import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { useState } from "react";

const AddUserModal = ({ onAdd }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [status, setStatus] = useState("");
  const [userName, setUserName] = useState("");
  const [selectedDate, setSelectedDate] = useState(null);
  const [registrationNumber, setRegistrationNumber] = useState("");

  const createdDate = new Date(selectedDate).toISOString();

  const onSubmit = (e) => {
    e.preventDefault();

    if (
      !firstName ||
      !lastName ||
      !status ||
      !userName ||
      !selectedDate ||
      !registrationNumber
    ) {
      alert("veuillez remplir tous les champs");
      return;
    }

    onAdd({
      firstName,
      lastName,
      status,
      userName,
      createdDate,
      registrationNumber,
    });

    setFirstName("");
    setLastName("");
    setStatus("");
    setUserName("");
    setSelectedDate(null);
    setRegistrationNumber("");
  };

  return (
    <div className="modal_form">
      <h1> Ajout d'utilisateur</h1>
      <form action="#" className="form" onSubmit={onSubmit} autoComplete="off">
        <div className="row1">
          <div className="wrapper">
            <label htmlFor="firstName">Nom</label>
            <input
              type="text"
              id="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="wrapper">
            <label htmlFor="lastName">Prenom</label>
            <input
              type="text"
              id="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div className="wrapper">
            <label htmlFor="status">Etat</label>
            <select
              className="selectInput"
              id="status"
              value={status}
              onChange={(e) => setStatus(e.target.value)}
            >
              <option selected hidden></option>
              <option>En validation</option>
              <option>Validé</option>
              <option>Rejeté</option>
            </select>
          </div>
        </div>
        <div className="row2">
          <div className="wrapper">
            <label htmlFor="userName">Nom d'utilisateur</label>
            <input
              type="text"
              id="userName"
              value={userName}
              onChange={(e) => setUserName(e.target.value)}
            />
          </div>
          <div className="wrapper">
            <label htmlFor="dateInput">Date de creation</label>
            <DatePicker
              className="dateInput"
              dateFormat="yyyy/MM/dd"
              selected={selectedDate}
              value={selectedDate}
              onChange={(date) => setSelectedDate(date)}
            />
          </div>
        </div>
        <div className="row3">
          <div className="wrapper">
            <label htmlFor="registrationNumber">Matricule</label>
            <input
              type="number"
              id="registrationNumber"
              value={registrationNumber}
              onChange={(e) => setRegistrationNumber(e.target.value)}
            />
          </div>
        </div>
        <div className="row_bottom">
          <button type="submit" className="submitBtn">
            Enregistrer
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddUserModal;
