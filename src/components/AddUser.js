const AddUser = ({ onOpen }) => {
  return (
    <div className="addUser">
      <button className="openModalBtn" onClick={onOpen}>
        Ajouter utilisateur
      </button>
    </div>
  );
};

export default AddUser;
