import { FaTrash } from "react-icons/fa";
import Etat from "./Etat";

const User = ({ user, onDelete }) => {
  return (
    <>
      <tr>
        <td>{user.id}</td>
        <td>
          {("0" + new Date(user.createdDate).getDate()).slice(-2) +
            "/" +
            ("0" + (new Date(user.createdDate).getMonth() + 1)).slice(-2) +
            "/" +
            new Date(user.createdDate).getFullYear()}
        </td>
        <td>
          <Etat
            etat={user.status}
            color={(() => {
              switch (user.status) {
                case "En validation":
                  return "#FDB64D";
                case "Validé":
                  return "#5BE881";
                case "Rejeté":
                  return "#FF0000";
                default:
                  return;
              }
            })()}
          />
        </td>
        <td>{user.firstName}</td>
        <td>{user.lastName}</td>
        <td>{user.userName}</td>
        <td>{user.registrationNumber}</td>
        <td>
          <FaTrash
            onClick={() => onDelete(user.id)}
            style={{
              cursor: "pointer",
            }}
          />
        </td>
      </tr>
    </>
  );
};

export default User;
