const Etat = ({ etat, color }) => {
  const style = {
    backgroundColor: color,
    borderRadius: "5px",
    color: "white",
    padding: "5px",
    textAlign: "center",
    fontSize: "10px",
    display: "block",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
    width: " 50%",
  };
  return (
    <lebel style={style} htmlFor="etat">
      {etat}
    </lebel>
  );
};

export default Etat;
