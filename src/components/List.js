import User from "./User";

const List = ({ usersData, onDelete }) => {
  return (
    <div className="container">
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Date de création</th>
            <th>Etat</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Nom d'utilisateur</th>
            <th>Matricule</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {usersData.map((user) => {
            return <User key={user.id} user={user} onDelete={onDelete} />;
          })}
        </tbody>
      </table>
    </div>
  );
};

export default List;
