import "./App.css";
import List from "./components/List";
import { useState } from "react";
import Modal from "react-modal";
import AddUser from "./components/AddUser";
import AddUserModal from "./components/AddUserModal";

Modal.setAppElement("#root");
function App() {
  const [users, setUsers] = useState([
    {
      id: "123456789",
      createdDate: "2021-01-06T00:00:00.000Z",
      status: "En validation",
      firstName: "Mohamed",
      lastName: "Taha",
      userName: "mtaha",
      registrationNumber: "2584",
    },
    {
      id: "987654321",
      createdDate: "2021-07-25T00:00:00.000Z",
      status: "Validé",
      firstName: "Hamid",
      lastName: "Orrich",
      userName: "horrich",
      registrationNumber: "1594",
    },
    {
      id: "852963741",
      createdDate: "2021-09-15T00:00:00.000Z",
      status: "Rejeté",
      firstName: "Rachid",
      lastName: "Mahidi",
      userName: "rmahidi",
      registrationNumber: "3576",
    },
  ]);

  const [modalIsOpen, setModalIsOpen] = useState(false);

  // open modal
  const openModal = () => {
    setModalIsOpen(true);
  };
  // close modal
  const closeModal = () => {
    setModalIsOpen(false);
  };
  // Delete user
  const deleteUser = (id) => {
    setUsers(users.filter((user) => user.id !== id));
  };

  // Add new user
  const addUser = (user) => {
    const id = Math.floor(Math.random() * 234567890) + 1;
    const newUser = { id, ...user };
    setUsers([...users, newUser]);
  };
  return (
    <div className="App">
      <List usersData={users} onDelete={deleteUser} />
      <AddUser onOpen={openModal} />
      <Modal
        className="modalStyle"
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={{
          overlay: { backgroundColor: "rgba(99, 99, 99, 0.7)" },
          content: {},
        }}
      >
        <AddUserModal onAdd={addUser} />
      </Modal>
    </div>
  );
}

export default App;
